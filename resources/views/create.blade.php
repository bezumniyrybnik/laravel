@extends('layouts.form')

@section('title', 'Добавление статьи')

@section('form')

    <div class="articles">
        <form method="post" action="{{ route('send') }}" class="decor">
            <div class="form-inner">
                <h3>Добавление статьи</h3>
                <input type="text" class="form-control {{ $errors->has('title') ? 'is-invalid' : ''}}" name="title"
                       placeholder="Название" value="{{ old('title') }}">
                @error('title')
                <div class="text-danger">{{ $message }}</div>
                @enderror
                <select class="form-control mb-2">
                    @foreach($categories as $category)
                        <option>{{$category->category}}</option>
                    @endforeach
                </select>
                <textarea class="form-control {{ $errors->has('content') ? 'is-invalid' : ''}}" name="content"
                          placeholder="Содержимое" rows="5" value="{{ old('content') }}"></textarea>
                @error('content')
                <div class="text-danger">{{ $message }}</div>
                @enderror
                <input type="hidden" name="_token" value="{{ csrf_token() }}" required>
                <input type="submit" value="Отправить">
                <a href="{{route('all')}}">Назад</a>
            </div>
        </form>
    </div>

    {{--<div class="container">
        <form>
            <div class="form-group">
                <label for="exampleInputEmail1">Название статьи</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Содержимое статьи</label>
                <input type="password" class="form-control" id="exampleInputPassword1">
            </div>
            <button type="submit" class="btn btn-primary">Разместить</button>
        </form>
    </div>--}}

@endsection
