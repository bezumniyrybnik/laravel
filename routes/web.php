<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// получение всех статтей
Route::get('/', [
    'as' => 'all',
    'uses' => 'ArticlesController@all'
]);

// получение одной статьи
Route::get('/article/{article}', [
    'as' => 'get',
    'uses' => 'ArticlesController@get'
]);

// получение статтей по категориям
Route::get('/category/{category}', [
    'as' => 'category',
    'uses' => 'ArticlesController@category'
]);

// получение статтей пользователя
Route::get('/user/{user}', [
    'as' => 'user',
    'uses' => 'ArticlesController@user'
]);

// таблица управления статьями
Route::get('/control', [
    'as' => 'control',
    'uses' => 'ArticlesController@control'
]);

// форма для новой статьи
Route::get('/create', [
    'as' => 'create',
    'uses' => 'ArticlesController@formCreate'
]);

// отправка новой статьи
Route::post('/send', [
    'as' => 'send',
    'uses' => 'ArticlesController@new'
]);

// форма для редактирование
Route::get('/edit/{article}', [
    'as' => 'edit',
    'uses' => 'ArticlesController@formEdit'
]);

// редактирование статьи
Route::put('/change/{article}', [
    'as' => 'change',
    'uses' => 'ArticlesController@edit'
]);

// удаление статьи
Route::delete('/delete/{article}', [
    'as' => 'delete',
    'uses' => 'ArticlesController@delete'
]);

// отправка нового комментария
Route::post('/comment', [
    'as' => 'comment',
    'uses' => 'CommentController@new'
]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
