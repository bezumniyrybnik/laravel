<?php


namespace App\Http\Controllers;


class UserController
{
    public function login()
    {
        return view('auth.login');
    }

    public function register()
    {
        return view('auth.register');
    }


}
