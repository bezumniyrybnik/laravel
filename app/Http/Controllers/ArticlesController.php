<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBlogPost;
use App\Models\Article;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Tag;
use App\User;
use Carbon\Carbon;

class ArticlesController extends Controller
{
    // отображение всех статтей
    public function all()
    {
        return view('welcome', [
            'articles' => Article::query()->where('updated_at', '<=', Carbon::now())->paginate(4),
            'categories' => Category::all(),
            'tags' => Tag::all(),
        ]);
    }

    // отображение статтей по категориям
    public function category(Category $category)
    {
        $articles = Article::query()->where('category_id', '=', $category->id)->get();
        return view('category', ['articles' => $articles, 'category' => $category]);
    }

    // отображение статтей юзера
    public function user(User $user)
    {
        $articles = Article::query()->where('user_id', '=', $user->id)->get();
        return view('user', ['articles' => $articles, 'user' => $user]);
    }

    // отображение управления статьями
    public function control()
    {
        return view('control', [
            'articles' => Article::query()->where('user_id', '=', \Auth::id())->get()
        ]);
    }

    // отображение одной статьи
    public function get(Article $article)
    {
        $comments = Comment::where('articles_id', '=', $article->id)->get();
        $otherArticles = Article::where('id', '!=', $article->id)->inRandomOrder()->limit(3)->get();
        return view('article', [
            'article' => $article,
            'categories' => $article->category->category,
            'username' => $article->user->name,
            'comments' => $comments,
            'otherArticles' => $otherArticles,
        ]);
    }

    // создание новой статьи
    public function new(StoreBlogPost $request)
    {
        dd($request->input('tag_id'));
        //s$tags = $request->input('id_tag');
        $article = new Article($request->validated());
        if ( есть ли данные в request)
        $article->tags()->sync($article->id);
        \Auth::user()->articles()->save($article);
        return redirect()->route('all');
    }

    // редактирование статьи
    public function edit(StoreBlogPost $request, Article $article)
    {
        $article->fill($request->validated());
        $article->save();
        return view('edit', ['article' => $article]);
    }

    // удаление статьи
    public function delete(Article $article)
    {
        Article::destroy($article->id);
        return redirect()->route('all');
    }

    // форма для создание статьи
    public function formCreate()
    {
        return view('create');
    }

    // форма для редактирование статьи
    public function formEdit(Article $article)
    {
        return view('edit', ['article' => $article]);
    }
}
