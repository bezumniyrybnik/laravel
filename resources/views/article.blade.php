@extends('layouts.app')

@section('title', 'читаем')

@section('content')

    {{--
        <div class="modal fade" id="editArticle">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Добавление статьи</h5>
                        <button class="close" data-dismiss="modal">
                            <span>&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{ route('change', $article) }}">
                            <input type="text" class="form-control mb-2 {{ $errors->has('title') ? 'is-invalid' : ''}}"
                                   name="title" value="{{ $article->title }}">
                            @error('title')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                            <textarea class="form-control mb-2 {{ $errors->has('content') ? 'is-invalid' : ''}}"
                                      name="content" rows="10">{{ $article->content }}</textarea>
                            @error('content')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" required>
                            <input type="hidden" name="_method" value="PUT">
                            <input type="submit" value="Опубликовать">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button href='{{ route('all') }}'><a class="btn-primary">Назад</a></button>
                    </div>
                </div>
            </div>
        </div>
    --}}
    <div class="container text-center">
        <div class="card mb-3">
            <h3 class="card-header">{{ $article->title }}</h3><br>
            <h5>Категория: <a href="#">{{ $categories }}</a></h5>
            <p class="card-text">{{ $article->content }}</p><br>
            <h4 class="card-text text-right">Автор: <a href="#{{-- route('user', $article) --}}">{{ $username }}</a> <i
                    class="fas fa-check-circle"></i></h4>
            <p>{{ $article->updated_at }}</p>
            <div class="tags">
                Теги:
                @foreach($article->tags as $tag)
                    <a href="#">{{ $tag->name }} </a>
                @endforeach
            </div>
        </div>

        <div class="comment">
            <h4>Комментарии:</h4>

            @auth()
                <form method="post" action="{{ route('comment') }}">
                    <input type="text" class="form-control mb-2" name="comment">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" required>
                    <input type="hidden" name="articles_id" value="{{ $article->id }}">

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <input type="submit" value="Отправить">
                </form>
            @endauth

            @foreach($comments as $comment)
                <div class="card mt-2 mb-1">
                    <h4 class="card-text"><a href="#">{{ $comment->user->name }}</a>: {{ $comment->comment }}</h4>
                    <em>{{$comment->updated_at}}</em>
                </div>
            @endforeach
        </div>

        <div class="otherArticle">
            <h3 class="mt-3">Другие похожие статьи:</h3>
            @foreach($otherArticles as $article)
                <div class="card mb-3">
                    <h5 class="card-header">{{ $article->title }}</h5>
                    <div class="card-body">
                        <p class="card-text">{{ $article->content }}</p>
                        <a href="{{ $article->link() }}" class="btn btn-primary">Читать</a>
                    </div>
                </div>
            @endforeach
        </div>

    </div>

@endsection
