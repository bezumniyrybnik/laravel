@extends('layouts.app')

@section('title', 'главная')

@section('content')

    <div class="container text-center">

        <div class="mb-3 mt-3">
            @auth()
                <a type="button" class="btn btn-success" href="{{ route('control') }}">Управление статьями</a>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newArticle">
                    <i class="fas fa-plus-square"></i>
                    Добавление статьи
                </button>
            @endauth
        </div>

        <div class="modal fade" id="newArticle">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Добавление статьи</h5>
                        <button class="close" data-dismiss="modal">
                            <span>&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{ route('send') }}">
                            <input type="text" class="form-control mb-2 {{ $errors->has('title') ? 'is-invalid' : ''}}"
                                   name="title" placeholder="Название">
                            @error('title')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                            <select name="category_id" class="form-control mb-2">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->category}}</option>
                                @endforeach
                            </select>
                            <textarea class="form-control mb-2 {{ $errors->has('content') ? 'is-invalid' : ''}}"
                                      name="content"
                                      placeholder="Содержимое" rows="7"></textarea>
                            @error('content')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                            <select name="tag_id[]" class="form-control mb-2" multiple size="3">
                                @foreach($tags as $tag)
                                    <option value="{{$tag->id}}">{{$tag->name}}</option>
                                @endforeach
                            </select>
                            <input type="date" name="updated_at" class="form-control">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" required>
                            <input type="submit" value="Опубликовать">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button href='{{ route('all') }}'><a href="">Назад</a></button>
                    </div>
                </div>
            </div>
        </div>

        @foreach($articles as $article)
            <div class="card mb-3">
                <h5 class="card-header">{{ $article->title }}</h5>
                <div class="card-body">
                    <p class="card-text">{{ $article->content }}</p>
                    <a href="{{ $article->link() }}" class="btn btn-primary">Читать</a>
                </div>
            </div>
        @endforeach

        <div class="btn-group" role="group" aria-label="First group">
            {{ $articles->links() }}
        </div>

    </div>

@endsection
