@extends('layouts.app')

@section('title', 'Редактирование статьи')

@section('form')

    <div class="container">
        <form method="post" action="{{ route('change', $article) }}">
            <div class="form-group">
                <h3>Редактирование статьи</h3>
                <input type="text" class="form-control mb-2 {{ $errors->has('title') ? 'is-invalid' : ''}}" name="title"
                       value="{{ $article->title }}" required>
                @error('title')
                    <div class="text-danger mb-2">{{ $message }}</div>
                @enderror
                <textarea class="form-control mb-2 {{ $errors->has('content') ? 'is-invalid' : ''}}" name="content"
                          rows="7" required>{{ $article->content }}</textarea>
                @error('content')
                    <div class="text-danger mb-2">{{ $message }}</div>
                @enderror
                <input type="hidden" name="_token" value="{{ csrf_token() }}" required>
                <input type="hidden" name="_method" value="PUT">
                <input type="submit" value="Отправить">
                <a href="{{route('all')}}">Назад</a>
            </div>
        </form>
    </div>

@endsection
