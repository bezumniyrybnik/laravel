@extends('layouts.app')

@section('title', 'управление')

@section('control')

    <div class="container text-center">

        <h2 class="mb-3">Управление статьями</h2>

        @foreach($articles as $article)
            <div class="card mb-3">
                <h5 class="card-header">{{ $article->title }}</h5>
                <div class="card-body text-center">
                    <p class="card-text">{{ $article->content }}</p>
                    <em>{{$article->updated_at}}</em>
                    <div class="btn-toolbar mb-3" role="toolbar">


                        <li><a class="btn btn-primary mr-4" href="{{ $article->link() }}">Читать <i
                                    class="fas fa-book"></i></a></li>
                        <li><a class="btn btn-success mr-4" href="{{ route('edit', $article) }}">Редактировать <i
                                    class="fas fa-edit"></i></a>
                            {{--<button type="button" class="btn btn-success" data-toggle="modal" data-target="#editArticle">
                                Редактирование статьи
                            </button>--}}
                        </li>
                        <li>
                            <form method="post" action="{{ route('delete', $article) }}" class="decor">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" required>
                                <input type="submit" class="btn btn-danger" value="Удалить">
                            </form>
                        </li>

                        <div class="ml-4">
                            @if($article->updated_at >= \Carbon\Carbon::now())
                                <h4>Состояние: статья не опубликована</h4>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        @endforeach

    </div>

    {{--<div class="modal fade" id="editArticle">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Добавление статьи</h5>
        <button class="close" data-dismiss="modal">
            <span>&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <form method="post" action="{{ route('change', $article) }}">
            <input type="text" class="form-control mb-2 {{ $errors->has('title') ? 'is-invalid' : ''}}"
                   name="title" value="{{ $article->title }}">
            @error('title')
            <div class="text-danger">{{ $message }}</div>
            @enderror
            <textarea class="form-control mb-2 {{ $errors->has('content') ? 'is-invalid' : ''}}"
                      name="content" rows="10">{{ $article->content }}</textarea>
            @error('content')
            <div class="text-danger">{{ $message }}</div>
            @enderror
            <input type="hidden" name="_token" value="{{ csrf_token() }}" required>
            <input type="hidden" name="_method" value="PUT">
            <input type="submit" value="Опубликовать">
        </form>
    </div>
    <div class="modal-footer">
        <button href='{{ route('all') }}'><a href="">Назад</a></button>
    </div>
    </div>
    </div>
    </div>--}}


@endsection
