<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBlogPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|unique:articles|min:5|max:255',
            'category_id' => 'required',
            'content' => 'required|min:10',
            'updated_at' => 'date',
            'tag_id' => 'sometime|array'
        ];
    }

    /*public function messages()
    {
        return [
            'title.required' => 'Необходимо указать залоговок статьи',
            'title.unique' => 'Такое название уже существует',
            'title.min' => 'Необходимо написать больше 5 символов',
            'title.max' => 'Вы превысили заданный размер (больше 255 символов)',
            'content.required' => 'Необходимо указать содержимое статьи',
            'content.min' => 'Необходимо написать больше 10 символов',
        ];
    }*/
}
