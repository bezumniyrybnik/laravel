@if($data)

    <div class="categories">

        @foreach($data as $category)
            <a class="p-2 text-muted col-sm" href="{{ $category->link() }}">{{ $category->category }}</a>
        @endforeach

    </div>

@endif
